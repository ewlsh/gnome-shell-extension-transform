const ExtensionUtils = imports.misc.extensionUtils;

const Me = ExtensionUtils.getCurrentExtension();

class DashToDock_DockManager {
  constructor() {
    if (Me.imports.extension.dockManager) throw new Error("DashToDock has been already initialized");

    Me.imports.extension.dockManager = this;
  }

  static getDefault() {
    return Me.imports.extension.dockManager;
  }
}

export { DashToDock_DockManager as DockManager };
Signals.addSignalMethods(DashToDock_DockManager.prototype);
