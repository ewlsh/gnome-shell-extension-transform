import * as Extension from './extension.js'
import * as ExtensionUtils from "resource:///org/gnome/shell/misc/extensionUtils.js";

const Me = ExtensionUtils.getCurrentExtension();

class DashToDock_DockManager {
  constructor() {
    //TODO(codemod): This was originally an inline import of Me.imports.extension
    if (Extension.dockManager)
      throw new Error("DashToDock has been already initialized");

    //TODO(codemod): This was originally an inline import of Me.imports.extension
    Extension.dockManager = this;
  }

  static getDefault() {
    //TODO(codemod): This was originally an inline import of Me.imports.extension
    return Extension.dockManager;
  }
}

export { DashToDock_DockManager as DockManager };
Signals.addSignalMethods(DashToDock_DockManager.prototype);
