import Clutter from "gi://Clutter";
import * as Params from "resource:///org/gnome/shell/misc/params.js";
import * as Main from "resource:///org/gnome/shell/ui/main.js";
import * as ExtensionUtils from "resource:///org/gnome/shell/misc/extensionUtils.js";

const Me = ExtensionUtils.getCurrentExtension();
import * as Extension from './extension.js'
import * as AppSpread from "./appSpread.js";

const DOCK_DWELL_CHECK_INTERVAL = 100;
const ICON_ANIMATOR_DURATION = 3000;

export const State = {
    HIDDEN: 0,
    SHOWING: 1,
    SHOWN: 2,
    HIDING: 3,
};

export const DashSlideContainer = GObject.registerClass(
    {},
    class DashSlideContainer extends St.Bin {
        _init(params = {}) {
            super._init(params);
        }
    }
);

export const DockedDash = GObject.registerClass(
    {},
    class DashToDock extends St.Bin {
        _init(params) {
            this._position = Utils.getPosition();

            // This is the centering actor
            super._init({
                ...params,
                name: "dashtodockContainer",
                reactive: false,
                style_class: Theming.PositionStyleClass[this._position],
            });

            if (this.monitorIndex === undefined) {
                // Hello turkish locale, gjs has instead defined this.monitorİndex
                // See: https://gitlab.gnome.org/GNOME/gjs/-/merge_requests/742
                this.monitorIndex = this.monitor_index;
            }
        }
    }
);

const DashToDock_KeyboardShortcuts_NUM_HOTKEYS = 10;

export const CONSTANT = 10;

class DashToDock_KeyboardShortcuts {
    constructor() {}
}

export { DashToDock_KeyboardShortcuts as KeyboardShortcuts };

class DashToDock_WorkspaceIsolation {
    constructor() {}
}

export { DashToDock_WorkspaceIsolation as WorkspaceIsolation };

class X {
  
}
export { X as X_3 }

export class DmManager {
}

export class DxManager {
}

export function x() {
}

class DashToDock_DockManager {
    constructor() {
        //TODO(codemod): This was originally an inline import of Me.imports.extension
        if (Extension.dockManager)
            throw new Error("DashToDock has been already initialized");

        //TODO(codemod): This was originally an inline import of Me.imports.extension
        Extension.dockManager = this;
    }

    static getDefault() {
        //TODO(codemod): This was originally an inline import of Me.imports.extension
        return Extension.dockManager;
    }

    static get allDocks() {
        return DashToDock_DockManager.getDefault()._allDocks;
    }

    static get settings() {
        return DashToDock_DockManager.getDefault().settings;
    }

    get settings() {
        return this._settings;
    }

    static get iconTheme() {
        return DashToDock_DockManager.getDefault().iconTheme;
    }

    get settings() {
    // eslint-disable-line no-dupe-class-members
        return this._settings;
    }

    get iconTheme() {
        return this._iconTheme.iconTheme;
    }

    get fm1Client() {
        return this._fm1Client;
    }

    get remoteModel() {
        return this._remoteModel;
    }

    get mainDock() {
        return this._allDocks.length ? this._allDocks[0] : null;
    }

    get removables() {
        return this._removables;
    }

    get trash() {
        return this._trash;
    }

    get desktopIconsUsableArea() {
        return this._desktopIconsUsableArea;
    }

    get discreteGpuAvailable() {
        return AppDisplay.discreteGpuAvailable || this._discreteGpuAvailable;
    }

    get appSpread() {
        return this._appSpread;
    }

    getDockByMonitor(monitorIndex) {
        return this._allDocks.find((d) => d.monitorIndex === monitorIndex);
    }

    _ensureLocations() {}
}

export { DashToDock_DockManager as DockManager };
Signals.addSignalMethods(DashToDock_DockManager.prototype);

class DashToDock_IconAnimator {
    constructor(actor) {}
}

export { DashToDock_IconAnimator as IconAnimator };

export function doSomething() {}
