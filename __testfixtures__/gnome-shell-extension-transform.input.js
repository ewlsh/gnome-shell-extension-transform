// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-

const Clutter = imports.gi.Clutter;

const Params = imports.misc.params;

const Main = imports.ui.main;

const ExtensionUtils = imports.misc.extensionUtils;

const Me = ExtensionUtils.getCurrentExtension();
const AppSpread = Me.imports.appSpread;

const DOCK_DWELL_CHECK_INTERVAL = 100;
const ICON_ANIMATOR_DURATION = 3000;

var State = {
    HIDDEN: 0,
    SHOWING: 1,
    SHOWN: 2,
    HIDING: 3,
};

var DashSlideContainer = GObject.registerClass(
    {},
    class DashSlideContainer extends St.Bin {
        _init(params = {}) {
            super._init(params);
        }
    }
);

var DockedDash = GObject.registerClass(
    {},
    class DashToDock extends St.Bin {
        _init(params) {
            this._position = Utils.getPosition();

            // This is the centering actor
            super._init({
                ...params,
                name: "dashtodockContainer",
                reactive: false,
                style_class: Theming.PositionStyleClass[this._position],
            });

            if (this.monitorIndex === undefined) {
                // Hello turkish locale, gjs has instead defined this.monitorİndex
                // See: https://gitlab.gnome.org/GNOME/gjs/-/merge_requests/742
                this.monitorIndex = this.monitor_index;
            }
        }
    }
);

const DashToDock_KeyboardShortcuts_NUM_HOTKEYS = 10;

var CONSTANT = 10;

var KeyboardShortcuts = class DashToDock_KeyboardShortcuts {
    constructor() {}
};

var WorkspaceIsolation = class DashToDock_WorkspaceIsolation {
    constructor() {}
};

class X {
  
}
export { X as X_3 }

var DmManager = class DmManager {
}

var DxManager = class {
}

function x() {
}

var DockManager = class DashToDock_DockManager {
    constructor() {
        if (Me.imports.extension.dockManager) throw new Error("DashToDock has been already initialized");

        Me.imports.extension.dockManager = this;
    }

    static getDefault() {
        return Me.imports.extension.dockManager;
    }

    static get allDocks() {
        return DockManager.getDefault()._allDocks;
    }

    static get settings() {
        return DockManager.getDefault().settings;
    }

    get settings() {
        return this._settings;
    }

    static get iconTheme() {
        return DockManager.getDefault().iconTheme;
    }

    get settings() {
    // eslint-disable-line no-dupe-class-members
        return this._settings;
    }

    get iconTheme() {
        return this._iconTheme.iconTheme;
    }

    get fm1Client() {
        return this._fm1Client;
    }

    get remoteModel() {
        return this._remoteModel;
    }

    get mainDock() {
        return this._allDocks.length ? this._allDocks[0] : null;
    }

    get removables() {
        return this._removables;
    }

    get trash() {
        return this._trash;
    }

    get desktopIconsUsableArea() {
        return this._desktopIconsUsableArea;
    }

    get discreteGpuAvailable() {
        return AppDisplay.discreteGpuAvailable || this._discreteGpuAvailable;
    }

    get appSpread() {
        return this._appSpread;
    }

    getDockByMonitor(monitorIndex) {
        return this._allDocks.find((d) => d.monitorIndex === monitorIndex);
    }

    _ensureLocations() {}
};
Signals.addSignalMethods(DockManager.prototype);

// This class drives long-running icon animations, to keep them running in sync
// with each other, and to save CPU by pausing them when the dock is hidden.
var IconAnimator = class DashToDock_IconAnimator {
    constructor(actor) {}
};

function doSomething() {}
