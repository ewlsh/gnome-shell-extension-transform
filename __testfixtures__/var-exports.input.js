var Enum = {
    HIDDEN: 0,
    SHOWING: 1,
    SHOWN: 2,
    HIDING: 3,
};

var CONSTANT = 10;

var ExportedClass = class {
}

var RegisteredClass = GObject.registerClass(
    {},
    class RegisteredClass extends St.Bin {
        _init(params = {}) {
            super._init(params);
        }
    }
);