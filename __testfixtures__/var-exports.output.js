export const Enum = {
    HIDDEN: 0,
    SHOWING: 1,
    SHOWN: 2,
    HIDING: 3,
};

export const CONSTANT = 10;

export class ExportedClass {
}

export const RegisteredClass = GObject.registerClass(
    {},
    class RegisteredClass extends St.Bin {
        _init(params = {}) {
            super._init(params);
        }
    }
);