jest.autoMockOff();
const defineTest = require("jscodeshift/dist/testUtils").defineTest;
defineTest(__dirname, "gnome-shell-extension-transform");

for (const test of [
  "extension-imports",
  "gi-imports",
  "inline-imports",
  "shell-imports",
  "var-exports",
  "var-named-class-expression-exports",
])
  defineTest(__dirname, "gnome-shell-extension-transform", null, test);
